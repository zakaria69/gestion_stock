<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Produit;
use App\Models\Historie;
use App\Models\Sortie;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits = Produit::all();
        return  response()->json($produits);
    }

    public function filterByCategory($category){
        $produits = Produit::where('categorie', $category)->get();
        return response()->json($produits);
    }
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'prod_name' => 'required|string|unique:produits',
            'categorie' => 'required|string',
            'quantite' => 'required|integer',
            'prix' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $produit = Produit::create([
            'prod_name' => $request->prod_name,
            'categorie' => $request->categorie,
            'quantite' => $request->quantite,
            'prix' => $request->prix,
        ]);

        // Generate token or any other post-registration logic

        return response()->json(['message' => 'Produit créé avec succés', 'produit' => $produit], 201);
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        return view('produits.edit', compact('produit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ref)
    {
        $request->validate([
            'prod_name' => 'required',
            'categorie' => 'required',
            'quantite' => 'required|integer',
            'prix' => 'required|integer',
        ]);
        
    
        $produit = Produit::findOrFail($ref);
        $produit->update($request->all());
        return response()->json(['message' => 'Produit updated successfully', 'produit' => $produit], 200);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function updateDevis(Request $request)
{
    $request->validate([
        'produits' => 'required|array',
        'produits.*.ref' => 'required|exists:produits,ref',
        'produits.*.prod_name' => 'required',
        'produits.*.categorie' => 'required',
        'produits.*.quantite' => 'required|integer',
        'produits.*.prix' => 'required|integer',
    ]);

    foreach ($request->produits as $produitData) {
        $produit = Produit::findOrFail($produitData['ref']);
        $produit->update($produitData);
    }

    return response()->json(['message' => 'Produits updated successfully'], 200);
}




    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function destroy($ref)
    {
        $produit = Produit::where('ref', $ref)->first();

        if (!$produit) {
            return response()->json(['message' => 'Produit not found'], 404);
        }

        $produit->delete();

        return response()->json(['message' => 'Produit deleted successfully'], 200);
    }

    
    public function historie(Request $request)
    {
        // Validate the request data
        $validatedData = $request->validate([
            'produit_ref' => 'required|exists:produits,ref',
            'action' => 'required|string',
            'data' => 'required'
        ]);
    
        try {
            // Create a new history record
            $history = new Historie();
            $history->produit_ref = $validatedData['produit_ref'];
            $history->action = $validatedData['action'];
            $history->data = $validatedData['data'];
            $history->save();
    
            // Return a response, could be the new history object or a success message
            return response()->json([
                'message' => 'History record created successfully',
                'history' => $history
            ], 201);
        } catch (\Exception $e) {
            // Handle the exception
            return response()->json([
                'message' => 'Failed to create history record',
                'error' => $e->getMessage()
            ], 500);
        }
    }


    public function indexHistorie(){
        $historyRecords = Historie::orderBy('created_at', 'desc')->get();

        return response()->json([
            'historie' => $historyRecords
        ]);
    }







    public function sortie(Request $request)
    {
        // Validate the request data
        $validatedStorieData = $request->validate([
            'produit_ref' => 'required|exists:produits,ref',
            'action' => 'required|string',
            'data' => 'required'
        ]);
    
        try {
            // Create a new sortie record
            $sortie = new Sortie();
            $sortie->produit_ref = $validatedStorieData['produit_ref'];
            $sortie->action = $validatedStorieData['action'];
            $sortie->data = $validatedStorieData['data'];
            $sortie->save();
    
            // Return a response, could be the new sortie object or a success message
            return response()->json([
                'message' => 'Sortie record created successfully',
                'sortie' => $sortie
            ], 201);
        } catch (\Exception $e) {
            // Handle the exception
            return response()->json([
                'message' => 'Failed to create sortie record',
                'error' => $e->getMessage()
            ], 500);
        }
    }


    
    public function indexSortie(){
        $sortieRecords = Sortie::orderBy('created_at', 'desc')->get();

        return response()->json([
            'sortie' => $sortieRecords
        ]);
    }
}