<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Historie extends Model
{
    
    protected $table = 'historie';

    // Fillable fields for mass assignment
    protected $fillable = ['produit_ref', 'action', 'data'];

    // If you want to use JSON casting for the 'data' field
    protected $casts = [
        'data' => 'array',
    ];

    // Define the relationship with the Produit model
    public function produit()
    {
        return $this->belongsTo(Produit::class, 'produit_ref');
    }
}