<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up()
    {
        Schema::create('sortie', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('produit_ref');
            $table->string('action');
            $table->json('data');
            $table->timestamps();

            $table->foreign('produit_ref')->references('ref')->on('produits')->onDelete('set null');
        });
    }

    public function down()
    {
        Schema::dropIfExists('sortie');
    }
};