-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 29 mars 2024 à 09:36
-- Version du serveur : 10.4.32-MariaDB
-- Version de PHP : 8.0.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestion_stock`
--

-- --------------------------------------------------------

--
-- Structure de la table `historie`
--

CREATE TABLE `historie` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `produit_ref` bigint(20) UNSIGNED NOT NULL,
  `action` varchar(255) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`data`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `historie`
--

INSERT INTO `historie` (`id`, `produit_ref`, `action`, `data`, `created_at`, `updated_at`) VALUES
(17, 101, 'Modifier', '\"{\\\"prod_name\\\":\\\"Bureau\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"121\\\",\\\"prix\\\":20}\"', '2024-03-27 12:41:53', '2024-03-27 12:41:53'),
(18, 113, 'supprimé', '\"{\\\"prod_name\\\":\\\"Nacelle\\\",\\\"categorie\\\":\\\"Manutention am\\u00e9nagement d\'espace\\\",\\\"quantite\\\":50,\\\"prix\\\":680}\"', '2024-03-27 12:42:18', '2024-03-27 12:42:18'),
(19, 101, 'Modifier', '\"{\\\"prod_name\\\":\\\"Bureau\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"600\\\",\\\"prix\\\":20}\"', '2024-03-27 12:42:28', '2024-03-27 12:42:28'),
(20, 101, 'Modifier', '\"{\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":600,\\\"prix\\\":20}\"', '2024-03-27 13:07:21', '2024-03-27 13:07:21'),
(21, 101, 'Modifier', '\"{\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"99\\\",\\\"prix\\\":\\\"1550\\\"}\"', '2024-03-27 13:07:37', '2024-03-27 13:07:37'),
(22, 177, 'creation', '\"{\\\"prod_name\\\":\\\"chaise iso65\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"5\\\",\\\"prix\\\":\\\"4\\\"}\"', '2024-03-27 13:17:46', '2024-03-27 13:17:46'),
(23, 177, 'supprimé', '\"{\\\"prod_name\\\":\\\"chaise iso65\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":5,\\\"prix\\\":4}\"', '2024-03-27 13:18:01', '2024-03-27 13:18:01'),
(24, 176, 'supprimé', '\"{\\\"prod_name\\\":\\\"chaise iso 2\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":5,\\\"prix\\\":7}\"', '2024-03-27 13:18:04', '2024-03-27 13:18:04'),
(25, 178, 'creation', '\"{\\\"prod_name\\\":\\\"chaise iso65\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"5\\\",\\\"prix\\\":\\\"6\\\"}\"', '2024-03-27 13:18:51', '2024-03-27 13:18:51'),
(26, 178, 'supprimé', '\"{\\\"prod_name\\\":\\\"chaise iso65\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":5,\\\"prix\\\":6}\"', '2024-03-27 13:19:03', '2024-03-27 13:19:03'),
(27, 102, 'Modifier', '\"{\\\"prod_name\\\":\\\"Armoire\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"125\\\",\\\"prix\\\":95}\"', '2024-03-28 07:25:05', '2024-03-28 07:25:05'),
(28, 101, 'Modifier', '\"{\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"200\\\",\\\"prix\\\":1550}\"', '2024-03-28 09:00:04', '2024-03-28 09:00:04'),
(29, 101, 'Modifier', '\"{\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"200\\\",\\\"prix\\\":1550}\"', '2024-03-28 09:00:10', '2024-03-28 09:00:10'),
(30, 101, 'Modifier', '\"{\\\"prod_name\\\":\\\"bureau\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":189,\\\"prix\\\":1550}\"', '2024-03-28 12:15:18', '2024-03-28 12:15:18');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(2, '2024_03_11_084013_create_produits_table', 2),
(3, '2024_03_11_084500_create_users_table', 2),
(4, '2024_03_11_144849_add_soft_deletes_to_users_table', 3),
(5, '2024_03_13_101403_create_sortie_table', 4),
(6, '2024_03_16_092256_creat_sortie_table', 5),
(7, '2024_03_18_101723_create_historie_table', 6),
(8, '2024_03_20_091257_create_users_table', 7),
(9, '2024_03_25_093343_create_sortie_table', 8),
(10, '2024_03_26_084321_create_historie_table', 9),
(11, '2024_03_26_102445_create_historie_table', 10),
(12, '2024_03_26_102844_create_historie_table', 11),
(13, '2024_03_26_104448_create_historie_table', 12),
(14, '2024_03_26_104655_create_historie_table', 13),
(15, '2024_03_26_105107_create_historie_table', 14),
(16, '2024_03_26_111344_create_historie_table', 15),
(17, '2024_03_28_091753_create_users_table', 16);

-- --------------------------------------------------------

--
-- Structure de la table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `token` varchar(64) NOT NULL,
  `abilities` text DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expires_at`, `created_at`, `updated_at`) VALUES
(1, 'App\\Models\\User', 17, 'ACCESS_TOKEN', '2617b5c54c0d9e4a735c65146dbf74c1c0e4d73eeacddf232b0b7181a9d38135', '[\"*\"]', NULL, NULL, '2024-03-15 07:57:16', '2024-03-15 07:57:16');

-- --------------------------------------------------------

--
-- Structure de la table `produits`
--

CREATE TABLE `produits` (
  `ref` bigint(20) UNSIGNED NOT NULL,
  `prod_name` varchar(255) NOT NULL,
  `categorie` varchar(255) NOT NULL,
  `quantite` int(11) NOT NULL,
  `prix` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `produits`
--

INSERT INTO `produits` (`ref`, `prod_name`, `categorie`, `quantite`, `prix`, `created_at`, `updated_at`) VALUES
(101, 'bureau', 'Mobilier de bureau', 189, 1550, '2024-03-12 09:37:34', '2024-03-28 12:15:17'),
(102, 'Armoire', 'Mobilier de bureau', 125, 95, '2024-03-12 09:37:34', '2024-03-28 07:25:04'),
(103, 'Fonteuie', 'Mobilier de bureau', 83, 55, '2024-03-12 09:37:34', '2024-03-28 09:01:30'),
(105, 'Mobilier metalic', 'Mobilier de bureau', 138, 189, '2024-03-12 09:37:34', '2024-03-27 13:18:26'),
(106, 'Léger', 'Rayonnage', 50, 65, '2024-03-12 09:37:34', '2024-03-27 12:25:41'),
(107, 'Mi-Lourd', 'Rayonnage', 485, 135, '2024-03-12 09:37:34', '2024-03-12 09:37:34'),
(108, 'Lourd', 'Rayonnage', 229, 142, '2024-03-12 09:37:34', '2024-03-27 13:14:21'),
(109, 'Sortie de caisse gauche', 'Supermarche', 321, 78, '2024-03-12 09:37:34', '2024-03-12 09:37:34'),
(110, 'sortie de caisse droit', 'Supermarche', 23, 290, '2024-03-12 09:37:34', '2024-03-12 09:37:34'),
(111, 'Transpalettes', 'Manutention aménagement d\'espace', 45, 350, '2024-03-12 09:37:34', '2024-03-12 09:37:34'),
(112, 'Gerbeur', 'Mobilier de bureau', 30, 560, '2024-03-12 09:37:34', '2024-03-27 10:22:23'),
(114, 'échafaudage', 'Manutention aménagement d\'espace', 17, 425, '2024-03-12 09:37:34', '2024-03-12 09:37:34'),
(122, 'chaise iso', 'Mobilier de bureau', 100, 190, '2024-03-12 12:22:30', '2024-03-12 12:22:30'),
(175, 'bureau luban', 'Mobilier de bureau', 50, 1500, '2024-03-27 13:06:42', '2024-03-27 13:06:42');

-- --------------------------------------------------------

--
-- Structure de la table `sortie`
--

CREATE TABLE `sortie` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `produit_ref` bigint(20) UNSIGNED NOT NULL,
  `action` varchar(255) NOT NULL,
  `data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL CHECK (json_valid(`data`)),
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `sortie`
--

INSERT INTO `sortie` (`id`, `produit_ref`, `action`, `data`, `created_at`, `updated_at`) VALUES
(1, 101, 'Sortie', '\"{\\\"produits\\\":[{\\\"ref\\\":101,\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"7\\\",\\\"prix\\\":1550}]}\"', '2024-03-28 08:40:25', '2024-03-28 08:40:25'),
(2, 101, 'Sortie', '\"{\\\"produits\\\":[{\\\"ref\\\":101,\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"42\\\",\\\"prix\\\":1550}]}\"', '2024-03-28 08:46:54', '2024-03-28 08:46:54'),
(3, 103, 'Sortie', '\"{\\\"produits\\\":[{\\\"ref\\\":101,\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"4\\\",\\\"prix\\\":1550},{\\\"ref\\\":103,\\\"prod_name\\\":\\\"Fonteuie\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"10\\\",\\\"prix\\\":55}]}\"', '2024-03-28 09:01:31', '2024-03-28 09:01:31'),
(4, 101, 'Sortie', '\"{\\\"produits\\\":[{\\\"ref\\\":101,\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"3\\\",\\\"prix\\\":1550}]}\"', '2024-03-28 09:09:22', '2024-03-28 09:09:22'),
(5, 101, 'Sortie', '\"{\\\"produits\\\":[{\\\"ref\\\":101,\\\"prod_name\\\":\\\"bureau luban\\\",\\\"categorie\\\":\\\"Mobilier de bureau\\\",\\\"quantite\\\":\\\"4\\\",\\\"prix\\\":1550}]}\"', '2024-03-28 09:54:45', '2024-03-28 09:54:45');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(255) NOT NULL DEFAULT 'user',
  `remember_token` varchar(100) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `historie`
--
ALTER TABLE `historie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Index pour la table `produits`
--
ALTER TABLE `produits`
  ADD PRIMARY KEY (`ref`);

--
-- Index pour la table `sortie`
--
ALTER TABLE `sortie`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `historie`
--
ALTER TABLE `historie`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `produits`
--
ALTER TABLE `produits`
  MODIFY `ref` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=179;

--
-- AUTO_INCREMENT pour la table `sortie`
--
ALTER TABLE `sortie`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
