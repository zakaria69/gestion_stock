import axios from "axios";


const axiosClient = axios.create({
    baseURL: `${import.meta.env.VITE_API_BASE_URL}/api`
})


// axiosClient.interceptors.request.use((config)=>{
//     const token =localStorage.getItem('ACCESS_TOKEN')
//     config.headers.Authorization = `Bearer ${token}`

//     return config
// })

// axiosClient.interceptors.response.use((response) => {
//     return response;
// }, (error) => {
//     try {
//         const { response } = error;
//         if (response.status === 401) {
//             localStorage.removeItem('ACCESS_TOKEN');
//             // Optionally redirect to login page or show a logout message
//         }   
//     } catch (e) {
//         console.log(e);
//     }
//     return Promise.reject(error); // Re-throw the error
// });

export default axiosClient;