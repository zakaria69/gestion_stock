import { Link, Navigate, Outlet } from "react-router-dom";



export default function DefaultLayout(){


    return( 
        <>
            <div id="defaultlayout" className="defaultHome">
                            <nav className="navbar navbar-expand no-print">
                                <div className="container">
                                  <img className="navbar-brand" src="/src/img/logo.png" alt="logo" />
                                </div>
                            </nav>
                    <div className="head-main">
                            <aside className="dashboard no-print">
                                    <ul className=" list-group list-group-flush">
                                        <li className="list-group-item ">
                                        <Link to={"/devis"}> <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-file-earmark-plus" viewBox="0 0 16 16">
                                                <path d="M8 6.5a.5.5 0 0 1 .5.5v1.5H10a.5.5 0 0 1 0 1H8.5V11a.5.5 0 0 1-1 0V9.5H6a.5.5 0 0 1 0-1h1.5V7a.5.5 0 0 1 .5-.5"/>
                                                <path d="M14 4.5V14a2 2 0 0 1-2 2H4a2 2 0 0 1-2-2V2a2 2 0 0 1 2-2h5.5zm-3 0A1.5 1.5 0 0 1 9.5 3V1H4a1 1 0 0 0-1 1v12a1 1 0 0 0 1 1h8a1 1 0 0 0 1-1V4.5z"/>
                                            </svg>
                                            <span className="dasch-items ms-2">Nouveau devis</span></Link>
                                        </li>
                                        <li className="list-group-item  mt-2">
                                        <Link to={"/stock"}><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-stack" viewBox="0 0 16 16">
                                                <path d="m14.12 10.163 1.715.858c.22.11.22.424 0 .534L8.267 15.34a.6.6 0 0 1-.534 0L.165 11.555a.299.299 0 0 1 0-.534l1.716-.858 5.317 2.659c.505.252 1.1.252 1.604 0l5.317-2.66zM7.733.063a.6.6 0 0 1 .534 0l7.568 3.784a.3.3 0 0 1 0 .535L8.267 8.165a.6.6 0 0 1-.534 0L.165 4.382a.299.299 0 0 1 0-.535z"/>
                                                <path d="m14.12 6.576 1.715.858c.22.11.22.424 0 .534l-7.568 3.784a.6.6 0 0 1-.534 0L.165 7.968a.299.299 0 0 1 0-.534l1.716-.858 5.317 2.659c.505.252 1.1.252 1.604 0z"/>
                                            </svg>
                                            <span className="dasch-items ms-2"> Stock</span></Link>
                                        </li>
                                    </ul>
                            </aside>
                        
                        <main className="main-default">
                            <Outlet/>
                        </main>
                    </div>
            </div>
        </>
    );

}