import { Navigate, createBrowserRouter } from "react-router-dom";
import NotFound from "./views/NotFound";
import DefaultLayout from "./components/DefaultLayout";
import Stock from "./views/Stock";
import Ajouter from "./views/Ajouter";
import Update from "./views/Update";
import Verification from "./views/Verifier";
import Devis from "./views/Devis";
import PdfDevis from "./views/PDFdevi";
import Historie from "./views/Historique";
import Sortie from "./views/Sortie";
import Modification from "./views/Modification";
import Delete from "./views/Delete";


const router = createBrowserRouter([
{
    path: '/',
    element: <DefaultLayout/>,
    children: [
        {
            path: '/',
            element: <Navigate to="/devis"/>
        },
        {
            path: '/devis',
            element: <Devis/>
        },
        {
            path: '/stock',
            element: <Stock/>,
            children:[
                {
                    path: '/stock/ajouter',
                    element: <Ajouter/>,
                },
                {
                    path: '/stock/modification',
                    element: <Modification/>,
                    children:[
                        {
                            path: '/stock/modification/update',
                            element: <Update/>,
                        },
                        {
                            path: '/stock/modification/supprimer',
                            element: <Delete/>,
                        },
                    ]
                },
                {
                    path: '/stock/verification',
                    element: <Verification/>,
                },
                {
                    path: '/stock/historique',
                    element: <Historie/>,
                },
                {
                    path: '/stock/sortie',
                    element: <Sortie/>,
                },
            ]
        },
        {
            path: '/pdf',
            element: <PdfDevis/>
        },
    ]
},
{
    path: '*',
    element: <NotFound/>
},
]);

export default router;