import { useRef , useState } from "react";
import axiosClient from "../axios-client";

export default function Ajouter(){
    const [produit , setProduit] = useState();
    const [successMessage , setSuccessMessage] = useState();
    const [errorMessage , setErrorMessage] = useState();

    const prodnom = useRef();
    const categorie  = useRef();
    const quantite  = useRef();
    const prix  = useRef();

    const handleOnSubmit = (event) =>{
        event.preventDefault();
        const payload = {
            prod_name: prodnom.current.value,
            categorie: categorie.current.value,
            quantite: quantite.current.value,
            prix: prix.current.value,
        }
        axiosClient.post('/produits/create', payload)
        .then(({data}) => {
            setProduit(data.produit);
            setSuccessMessage('Produit créé avec succés');
            setErrorMessage(null)

            const historiePayload = {
                produit_ref: data.produit.ref,
                action: 'creation',
                data: JSON.stringify(payload)
            };

            axiosClient.post('/historie/record', historiePayload)
            .then(response => {
                console.log('History record created')
            })
            .catch(error => {
                console.log('Error creating history record ' , error);
            });
        })
        .catch(err => {
            const response = err.response;
            if(response && response.status === 422) {
                console.error(response.data.error);
                setErrorMessage(response.data.error)
            } else {
                console.error('An error occurred');
               
            }
        });
    }

    return(
        <>
            <div>
                <form className="ajoutForm" onSubmit={handleOnSubmit}>
                    <div>
                        <div className="mb-5 text-center">
                            <h3>AJOUTER NOUVEAU PRODUIT</h3>
                        </div>
                        {errorMessage && <div className="alert alert-danger alert-addu" role="alert">
                        {Object.keys(errorMessage).map(key => (
                            <>
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                             <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z"/>
                             <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/>
                           </svg>
                              <span className='ms-2' key={key}>{errorMessage[key][0]}</span> <br />
                              </>))}
                        
                    </div>}
                    {successMessage && <div className="alert alert-success alert-addu" role="alert">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check2-circle" viewBox="0 0 16 16">
                            <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0"/>
                            <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0z"/>
                        </svg>
                        <span className="ms-2">{successMessage}</span>  
                    </div>}

                    
                        <div className="form-floating mb-3">
                                <input type="text" ref={prodnom} className="form-control" name='prod_name' />
                                <label >Nom de Produit :</label>
                            </div>
                            <div className="form-floating mb-3">
                                <select className="form-select" ref={categorie} id="floatingSelect" aria-label="Floating label select example">
                                    <option value="Mobilier de bureau">Mobilier De Bureau</option>
                                    <option value="Rayonnage">Rayonnage</option>
                                    <option value="Supermarche">Supermarche</option>
                                    <option value="Manutention aménagement d'espace">Manutention aménagement d'espace</option>                  
                                </select>
                                <label htmlFor="floatingSelect">Choisissez une catégorie</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="number" ref={quantite}  className="form-control" name='quantite' min={1}/>
                                <label>Quantité</label>
                            </div>
                            <div className="form-floating mb-3">
                                <input type="number" ref={prix}  className="form-control" name='prix' min={1}/>
                                <label>Prix</label>
                            </div>
                            <div >
                                <input type="submit" className="btn btn-danger ms-2" value={'Enregistrer'} />
                                <input type="reset" className="btn btn-danger ms-2" value={'Reset'} />
                            </div>
                        </div>
                </form>
            </div>
        </>
    );
}