import { useEffect, useState } from "react";
import axiosClient from "../axios-client";



export default function Delete(){
    const [produits, setProduits] = useState([]);
    const [selectedProduit, setSelectedProduit] = useState(null);
    const [selectedCategory, setSelectedCategory] = useState('');
    const [successMessage , setSuccessMessage] = useState();
    const [errorMessage , setErrorMessage] = useState();


    useEffect(() => {
        const endpoint = selectedCategory ? `/produits/category/${selectedCategory}` : '/produits';
        axiosClient.get(endpoint)
            .then(response => {
                setProduits(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the produits data:', error);
            });
    }, [selectedCategory]);

    const handleCategoryClick = (category) => {
        setSelectedCategory(category);
      };

    const handleSelectChange = (event) => {
        const produitRef = event.target.value;
        const produit = produits.find(p => p.ref.toString() === produitRef);
        setSelectedProduit(produit);
 
    };


    const handleDelete = (event) => {
        event.preventDefault();

        if (!selectedProduit || !selectedProduit.ref){
            setErrorMessage('Aucun produit sélectionné');
            return;
        }
        setErrorMessage('')
        if(!window.confirm('êtes-vous sûr de vouloir supprimer ce produit')){
            return;
        }
        if (selectedProduit) {
            const deleteProduit = {
                prod_name: selectedProduit.prod_name,
                categorie: selectedProduit.categorie,
                quantite:selectedProduit.quantite,
                prix: selectedProduit.prix,
            }
                // Record the deletion in the history table
                const historiePayload = {
                    produit_ref: selectedProduit.ref,
                    action: 'supprimé',
                    data: JSON.stringify(deleteProduit)
                };

                axiosClient.post('/historie/record', historiePayload) 
                    .then(() => {
                        console.log('History record created for deletion');
                        axiosClient.delete(`/delete/${selectedProduit.ref}`,deleteProduit)
                        .then(() => {
        
                            // Update the state to reflect the deletion
                            setProduits(produits.filter(p => p.ref !== selectedProduit.ref));
                            setSelectedProduit(null);
                            setSuccessMessage('Produit supprimé avec succès');
                        })
                        .catch(error => {
                            console.error('There was an error deleting the produit:', error);
                        });
                    })
                    .catch(error => {
                        console.error('Error creating history record for deletion:', error);
                    });
           
        }
    };

    return(
        <>
            <div>
                <form action="" onSubmit={handleDelete} className="update-form">
                    <h3 className="text-center mb-5">SUPPRIMER UN PRODUIT</h3>
                    {errorMessage && <div className="alert alert-danger alert-addu" role="alert">
                             <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                             <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z"/>
                             <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/>
                           </svg>
                              <span className='ms-2'>{errorMessage}</span>
                        
                    </div>}
                    {successMessage && <div className="alert alert-success alert-addu" role="alert">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-check2-circle" viewBox="0 0 16 16">
                            <path d="M2.5 8a5.5 5.5 0 0 1 8.25-4.764.5.5 0 0 0 .5-.866A6.5 6.5 0 1 0 14.5 8a.5.5 0 0 0-1 0 5.5 5.5 0 1 1-11 0"/>
                            <path d="M15.354 3.354a.5.5 0 0 0-.708-.708L8 9.293 5.354 6.646a.5.5 0 1 0-.708.708l3 3a.5.5 0 0 0 .708 0z"/>
                        </svg>
                         <span className="ms-2">{successMessage}</span>
                    </div>}
                    <div className='button-radio'>
                            <div>
                                <input type="radio"  className="btn-check" name="options-outlined" id="Mobilier De Bureau" autoComplete="off" onClick={() => handleCategoryClick('Mobilier De Bureau')}/>
                                <label className="btn btn-outline-danger" htmlFor="Mobilier De Bureau">Mobilier De Bureau</label>
                            </div>
                            <div>
                                <input type="radio" className="btn-check" onClick={() => handleCategoryClick('Rayonnage')} name="options-outlined" id="Rayonnage" autoComplete="off"/>
                                <label className="btn btn-outline-danger" htmlFor="Rayonnage">Rayonnage</label>
                            </div>
                            <div>
                                <input type="radio" className="btn-check" onClick={() => handleCategoryClick('Supermarche')} name="options-outlined" id="Supermarche" autoComplete="off"/>
                                <label className="btn btn-outline-danger" htmlFor="Supermarche">Supermarche</label>
                            </div>
                            <div>
                                <input type="radio" className="btn-check" onClick={() => handleCategoryClick("Manutention aménagement d'espace")} name="options-outlined" id="Manutention" autoComplete="off"/>
                                <label className="btn btn-outline-danger" htmlFor="Manutention">Manutention</label>
                            </div>
                       
                        </div>
                    <div className="form-floating mb-3">
                                <select className="form-control" onChange={handleSelectChange} value={selectedProduit?.ref || ''}>
                                    <option value="">Select un produit</option>
                                    {produits.map(produit => (
                                        <option key={produit.ref} value={produit.ref}>
                                            {produit.prod_name}
                                        </option>
                                    ))} 
                                </select>
                                <label>Produits :</label>
                        </div>
                        <div className="update-btn">
                                <input className="btn btn-danger " type="submit" name="" value={'Supprimer'}/>
                        </div>
                </form>
                
            </div>
        </>
    );
}