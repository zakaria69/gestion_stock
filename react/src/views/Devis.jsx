import React, { useState, useEffect } from 'react';
import axiosClient from '../axios-client';
import { Link, useNavigate } from 'react-router-dom';

export default function Devis() {
    const [produits, setProduits] = useState([]);
    const [selectedProduit, setSelectedProduit] = useState(null);
    const [inputQuantite, setInputQuantite] = useState('');
    const [tableData, setTableData] = useState([]);
    const [errorMessage, setErrorMessage] = useState(''); 
    const [nom, setNom] = useState(''); 
    const [ville, setVille] = useState(''); 
    const navigate = useNavigate();
    const [devisTable , setDevisTable] = useState(false);
    const [selectedCategory, setSelectedCategory] = useState('');

    useEffect(() => {
        const endpoint = selectedCategory ? `/produits/category/${selectedCategory}` : '/produits';
        axiosClient.get(endpoint)
            .then(response => {
                setProduits(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the produits data:', error);
            });
    }, [selectedCategory]);

    const handleSelectChange = (event) => {
        const produitRef = event.target.value;
        const produit = produits.find(p => p.ref.toString() === produitRef);
        setSelectedProduit(produit);
        setErrorMessage(''); 
    };

    const handleInputChange = (event) => {
        setInputQuantite(event.target.value);
        setErrorMessage(''); 
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (selectedProduit && inputQuantite) {
            const reste = selectedProduit.quantite - inputQuantite;
            if (selectedProduit.quantite === 0) {
                    setErrorMessage(`Cet article n'est plus disponible.`);
                    return; 
            }else if(reste < 0){
                setErrorMessage(`Error: La quantite maximale autorisée est : ${selectedProduit.quantite}`);
                return; 
            }
            const prht = selectedProduit.prix ;
            const prttc = parseFloat(prht) * 1.2;
            const totalHt = parseFloat(prht) * parseFloat(inputQuantite);
            const totalTtc = parseFloat(totalHt) * 1.2;
            const newData = {
                ref: selectedProduit.ref,
                prod_name: selectedProduit.prod_name,
                categorie:selectedProduit.categorie,
                quantite: inputQuantite,
                prht: prht,
                prttc: prttc.toFixed(2),
                totalHt: totalHt.toFixed(2),
                totalTtc: totalTtc.toFixed(2),
                reste: reste,
            };
            setTableData([...tableData, newData]);
            setInputQuantite(''); 
            setDevisTable(true)
        }
    };
    const tableOnSubmit = (e) =>{
        e.preventDefault();
        
        if(tableData.length === 0){
            alert("Veuillez ajouter des données dans le tableau avant de sauvgarder en tant que PDF")
            return;
        }
        if(!nom || !ville){
            alert('Veuillez remplir le nom du client et la ville avant de sauvegarder en tant que PDF')
            return;
        }

        const produitToUpdate = tableData.find((produit) =>  produit.ref === selectedProduit.ref);

        if(produitToUpdate){
            const updateProduits = {
                produits: tableData.map(data => ({
                    ref: data.ref, 
                    prod_name: data.prod_name,
                    categorie: data.categorie,
                    quantite: data.reste,
                    prix: data.prht,
                }))
            };
            const SortieProduits = {
                produits: tableData.map(data => ({
                    ref: data.ref, 
                    prod_name: data.prod_name,
                    categorie: data.categorie,
                    quantite: data.quantite,
                    prix: data.prht,
                }))
            };
        
            console.log(updateProduits);
        
            axiosClient.put('/devis', updateProduits)
            .then(response => {
                console.log('success', response);



                const historiePayload = {
                    produit_ref: selectedProduit.ref,
                    action: 'Sortie',
                    data: JSON.stringify(SortieProduits)
                };

                axiosClient.post('/sortie', historiePayload)
                .then(response => {
                    console.log('Sortie record created')
                    
                    navigate('/pdf' ,{state: {
                        tableData: tableData , nom , ville}});
                })
                .catch(error => {
                    console.log('Error creating sortie record ' , error);
                });



            })
            .catch(error => {
                console.error('Il y a eu une erreur lors de la mise à jour des produits:', error);
            });
        }
        
       
    };

    const handleCategoryClick = (category) => {
        setSelectedCategory(category);
      };

    return (
        <>
            <form onSubmit={handleSubmit}>
            <div className="bon-form container"> 
            <h2 className='text-center mb-5'>Nouveau Devis</h2>
                <div className="d-flex">  
                            <div className=" form-floating mb-3 me-1"> 
                                <input type="text" className="form-control"  name="first_name" onChange={(e)=>{setNom(e.target.value)}}/>
                                <label>Nom du client :</label>
                            </div>
                            <div className=" form-floating mb-3 ms-auto ">
                                <input type="text" className="form-control"  name="ville" onChange={(e)=>{setVille(e.target.value)}}/>
                                <label>Ville du client :</label>
                            </div>
                        </div>  
                    <div className='button-radio'>
                        <div>
                            <input type="radio"  className="btn-check" name="options-outlined" id="Mobilier De Bureau" autoComplete="off" onClick={() => handleCategoryClick('Mobilier De Bureau')}/>
                            <label className="btn btn-outline-danger" htmlFor="Mobilier De Bureau">Mobilier De Bureau</label>
                        </div>
                        <div>
                            <input type="radio" className="btn-check" onClick={() => handleCategoryClick('Rayonnage')} name="options-outlined" id="Rayonnage" autoComplete="off"/>
                            <label className="btn btn-outline-danger" htmlFor="Rayonnage">Rayonnage</label>
                        </div>
                        <div>
                            <input type="radio" className="btn-check" onClick={() => handleCategoryClick('Supermarche')} name="options-outlined" id="Supermarche" autoComplete="off"/>
                            <label className="btn btn-outline-danger" htmlFor="Supermarche">Supermarche</label>
                        </div>
                        <div>
                            <input type="radio" className="btn-check" onClick={() => handleCategoryClick("Manutention aménagement d'espace")} name="options-outlined" id="Manutention" autoComplete="off"/>
                            <label className="btn btn-outline-danger" htmlFor="Manutention">Manutention</label>
                        </div>
                    </div>


                    <div className="mb-3">
                        <div className="form-floating mb-3">
                            <select className="form-control" onChange={handleSelectChange}>
                                <option value="">Select un produit</option>
                                {produits.map(produit => (
                                    <option key={produit.ref} value={produit.ref}>
                                        {produit.prod_name}
                                    </option>
                                ))} 
                            </select>
                            <label>Produits :</label>
                        </div>
                        {errorMessage && <div className="alert alert-danger" role="alert">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-exclamation-triangle" viewBox="0 0 16 16">
                        <path d="M7.938 2.016A.13.13 0 0 1 8.002 2a.13.13 0 0 1 .063.016.15.15 0 0 1 .054.057l6.857 11.667c.036.06.035.124.002.183a.2.2 0 0 1-.054.06.1.1 0 0 1-.066.017H1.146a.1.1 0 0 1-.066-.017.2.2 0 0 1-.054-.06.18.18 0 0 1 .002-.183L7.884 2.073a.15.15 0 0 1 .054-.057m1.044-.45a1.13 1.13 0 0 0-1.96 0L.165 13.233c-.457.778.091 1.767.98 1.767h13.713c.889 0 1.438-.99.98-1.767z"/>
                        <path d="M7.002 12a1 1 0 1 1 2 0 1 1 0 0 1-2 0M7.1 5.995a.905.905 0 1 1 1.8 0l-.35 3.507a.552.552 0 0 1-1.1 0z"/>
                      </svg>
                         <span className='ms-2'>{errorMessage}</span> 
                          </div>}
                        <div className="form-floating mb-3">
                            <input type="number" className="form-control" value={inputQuantite} onChange={handleInputChange} min={1}/>
                            <label>Quantité :</label>
                        </div>
                        <div className="btn-valid">
                            <input type="submit" className="btn btn-danger" value=" Enregistrer"/>
                        </div>
                    </div>
                </div>
            </form>
            {devisTable && 
                    <form onSubmit={tableOnSubmit}>
                    <div className="bon-table container">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Ref</th>
                                <th scope="col">Produits</th>
                                <th scope="col">Quantité</th>
                                <th scope="col">PRHT Unitaire </th>
                                <th scope="col">PRTTC</th>
                                <th scope="col">Reste</th>
                            </tr>
                        </thead>
                        <tbody >
                            {tableData.map((data, index) => (
                                    <tr key={index}>
                                        <th scope="row">{data.ref}</th>
                                        <td>{data.prod_name}</td>
                                        <td>{data.quantite}</td>
                                        <td>{data.prht} DH</td>
                                        <td>{data.prttc} DH</td>
                                        <td>{data.reste}</td>
                                    </tr>          
                            ))}                          
                            </tbody>
                        </table>
                    <div className="btn-valid">
                        <input type="submit" className="btn btn-danger" value="Valider"/>
                    </div>
            </div>
        </form>
            }
            
        </>
    );
}




