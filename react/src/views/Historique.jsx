import React, { useState, useEffect } from 'react';
import axiosClient from '../axios-client';

export default function Historie() {
    const [history, setHistory] = useState([]);

    useEffect(() => {
        axiosClient.get('/historie/show')
            .then(response => {
                setHistory(response.data.historie);
            })
            .catch(error => {
                console.error('Error fetching history records', error);
            });
    }, []);

    // Function to render JSON data in table columns
    const renderJsonDataInColumns = (jsonData) => {
        try {
            const dataObject = JSON.parse(jsonData);
            return Object.entries(dataObject).map(([key, value], index) => (
                <td key={index}>{value.toString()}</td>
            ));
        } catch (error) {
            return <td>{jsonData}</td>;
        }
    };

    return (
        <div className="container mt-5">
            <h3 className="mb-5 text-center">Historique des Produits</h3>
            <div className="table-responsive">
                <table className="table table-striped  table-bordered">
                    <thead className="table-light">
                        <tr>
                            <th>ID</th>
                            <th>Référence du Produit</th>
                            {history.length > 0 && Object.keys(JSON.parse(history[0].data)).map((key, index) => {
                                if(index === 0){
                                    return <th key={index}>Nom du Produit</th>
                                }
                                return <th key={index}>{key}</th>
                            })}
                            <th>Action</th>
                            <th>Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        {history.map((record, index) => (
                            <tr key={index}>
                                <td>{record.id}</td>
                                <td>{record.produit_ref}</td>
                                {renderJsonDataInColumns(record.data)}
                                <td>{record.action}</td>
                                <td>{new Date(record.created_at).toLocaleString()}</td>
                            </tr>
                        ))}
                    </tbody>
                </table>
            </div>
        </div>
    );
}



