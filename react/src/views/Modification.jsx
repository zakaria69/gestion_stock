

import { Link, Outlet } from "react-router-dom";


export default function Modification(){
    
    
    return(
        <>
            <div>
                <div className="UpdateBtn">
                    <div className="d-grid gap-2 ms-3 col-2  ">
                      <Link to={'/stock/modification/update'}>  <button type="button" className="btn btn-danger" >Modifier</button></Link>
                    </div>
                    <div className="d-grid gap-2 ms-3 col-2 ">
                      <Link to={'/stock/modification/supprimer'}>  <button type="button" className="btn btn-danger" >Supprimer</button></Link>
                    </div>
                </div>
                <div>
                    <Outlet/>
                </div>
            </div>
        </>
    );
}
