import {usePDF} from 'react-to-pdf'
import { useRef } from "react";
import { useLocation } from "react-router-dom";


export default function PdfDevis() {
    const location = useLocation();
    const { tableData , nom , ville} = location.state || {};
    const ref = useRef();
    const { toPDF , targetRef} = usePDF({filename: `${nom}.pdf`}); 

    // Function to calculate the total HT and TTC
    const calculateTotals = (data) => {
        return data.reduce(
            (totals, item) => {
                totals.totalHt += parseFloat(item.totalHt);
                totals.totalTtc += parseFloat(item.totalTtc);
                return totals;
            },
            { totalHt: 0, totalTtc: 0 }
        );
    };
    const formatDate = () =>{
        const currentDate = new Date();
        return currentDate.toLocaleDateString('fr-MA', {
            year: 'numeric',
            month: 'long',
            day: 'numeric'
        }) 
    }

    // Calculate totals
    const totals = calculateTotals(tableData);

    return (<>
        <div className="pdf" ref={targetRef}>
            <div className="print-table-container" >
                <div className="mb-4">
                    <img src="./src/img/logo.png" alt="" />
                </div>
                <div className="pdf-devis">
                    <div className="devis-titre mb-4 ">
                        <h4>Devis: </h4>
                        <ul>
                            <li>{nom}</li>
                            <li>{ville}</li>
                            <li>{formatDate()}</li>
                        </ul>
                    </div>
                    <table className="pdf-table">
                                    <thead style={{ backgroundColor: "rgb(174, 202, 221)" }}>
                                        <tr>
                                            <th>REF</th>
                                            <th>DESIGNATION</th>
                                            <th>DIM/MM</th>
                                            <th>QTE</th>
                                            <th>Prix Unitaire <br /> (en chiffre)</th>
                                            <th>Prix Total HT</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {tableData.map((item, index) => (
                                            <tr key={index}>
                                                <td>{item.ref}</td>
                                                <td>{item.prod_name}</td>
                                                <td>{/* DIM/MM value here */}</td>
                                                <td className='text-center'>{item.quantite}</td>
                                                <td className='text-center'>{item.prht} DH</td>
                                                <td className='text-center'>{item.totalHt} DH</td>
                                            </tr>
                                        ))}
                                        <tr>
                                            <td colSpan={4}></td>
                                            <td style={{ backgroundColor: "#eb9797" }}>TOTAL HT</td>
                                            <td className='text-center'>{totals.totalHt.toFixed(2)} DH</td>
                                        </tr>
                                        <tr>
                                            <td colSpan={4}></td>
                                            <td style={{ backgroundColor: "#eb9797" }}>TOTAL TTC</td>
                                            <td className='text-center'>{totals.totalTtc.toFixed(2)} DH</td>
                                        </tr>
                                        <tr style={{backgroundColor: "rgb(174, 202, 221)"}}>
                                            <td className="text-center" colSpan={6}>TRANSPORT ET MONTAGE A VOTRE CHARGE</td>
                                        </tr >
                                        <tr style={{backgroundColor: "rgb(174, 202, 221)"}}>
                                            <td className="text-center" colSpan={6}>MODALITE DE PAIEMENT: 50% AVANCE-50% AVANT CHARGEMENT</td>
                                        </tr>
                                        <tr style={{backgroundColor: "rgb(174, 202, 221)"}}>
                                            <td className="text-center" colSpan={6}>OFFRE VALIBALE UNE SEMAINE</td>
                                        </tr>
                                        <tr style={{backgroundColor: "rgb(174, 202, 221)"}}>
                                            <td className="text-center" colSpan={6}>*LA FACTURE SERA ETABLIE EN FONCTION DES QUANTITES REELEMENT POSEES</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <div className="desc">
                                    <small>
                                        Riad sofia, lmm N°7 Appt. 110, Hay Hassani - Casablanca - Fixe : 05 22 87 44 97 - GSM : 06 79 89 41 64 <br />
                                            RC: 398535 - CNSS : 5894531 - IF : 25105197 - Patente: 36200536 - ICE : 002025657000032 <br />
                                                Banque CDM : 021780000008903015833373 - Capital : 300.00.00 DHS <br />
                                                    lmmagencement@gmail.com - www.lmmagencement.com
                                    </small>
                         </div>
                </div>
            </div>
           
        </div>
        <div className="btn-PDF">
                <button onClick={toPDF} className="btn btn-danger">telecharger PDF</button>
            </div>
        </>
    );
}








