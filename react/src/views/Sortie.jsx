import React, { useState, useEffect } from 'react';
import axiosClient from '../axios-client';


export default function Sortie() {
    const [sortie, setSortie] = useState([]);

    useEffect(() => {
        axiosClient.get('/sortie/index')
            .then(response => {
                const formattedData = response.data.sortie.map(item => ({
                    ...item,
                    data: JSON.parse(item.data) 
                }));
                setSortie(formattedData);
            })
            .catch(error => {
                console.error('Error fetching sortie records', error);
            });
    }, []);

    return (
        <div className={'container '}>
            <h2 className="mb-4 text-center">Enregistrements de sorties</h2>
            <table className={'table table-bordered table-hover '}>
                <thead className="thead-light text-center">
                    <tr>
                        <th scope="col">Ref du Produit </th>
                        <th scope="col">Nom du Produit </th>
                        <th scope="col">Categorie</th>
                        <th scope="col">Quantitie</th>
                        <th scope="col">Action</th>
                        <th scope="col">Date</th>
                    </tr>
                </thead>
                <tbody>
                    {sortie.map((sortieItem, index) => (
                        sortieItem.data.produits.map((produit, produitIndex) => (
                            <tr key={`${index}-${produitIndex}`}>
                              
                                <td className='text-center'>{produit.ref}</td>
                                <td>{produit.prod_name}</td>
                                <td>{produit.categorie}</td>
                                <td className='text-center'>{produit.quantite}</td>
                                <td>{sortieItem.action}</td>
                                <td>{new Date(sortieItem.created_at).toLocaleString()}</td>
                            </tr>
                        ))
                    ))}
                </tbody>
            </table>
        </div>
    );
}