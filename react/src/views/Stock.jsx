
import { Link, Outlet } from "react-router-dom";


export default function Stock(){
    
    
    return(
        <>
            <div>
                <div className="StockBtn">
                    <div className="d-grid gap-2 ms-3 col-2">
                      <Link to={'/stock/ajouter'}>  <button type="button" className="btn btn-danger" >Ajouter</button></Link>
                    </div>
                    <div className="d-grid gap-2 ms-3 col-2">
                      <Link to={'/stock/sortie'}>  <button type="button" className="btn btn-danger" >Sortie</button></Link>
                    </div>
                    <div className="d-grid gap-2 ms-3 col-2">
                    <Link to={'/stock/verification'}> <button type="button" className="btn btn-danger" >Verification</button></Link>
                    </div>
                    <div className="d-grid gap-2 ms-3 col-2">
                    <Link to={'/stock/modification'} ><button type="button" className="btn btn-danger" >Modification</button></Link>
                    </div>  
                    <div className="d-grid gap-2 ms-3 col-2">
                       <Link to={"/stock/historique"}> <button type="button" className="btn btn-danger" >Historique</button></Link>
                    </div>
                </div>
                <div>
                    <Outlet/>
                </div>
            </div>
        </>
    );
}



