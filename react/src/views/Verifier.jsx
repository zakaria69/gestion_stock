import React, { useState, useEffect } from 'react';
import axiosClient from '../axios-client';

const Verification = () => {
  const [produits, setProduits] = useState([]);
  const [selectedCategory, setSelectedCategory] = useState('');

  useEffect(() => {
    // Fetch all produits if no category is selected, otherwise fetch by category
    const endpoint = selectedCategory ? `/produits/category/${selectedCategory}` : '/produits';
    axiosClient.get(endpoint)
      .then(response => {
        setProduits(response.data);
      })
      .catch(error => {
        console.error('There was an error fetching the produits data:', error);
      });
  }, [selectedCategory]);

  const handleCategoryClick = (category) => {
    setSelectedCategory(category);
  };

  return (
    <div className='container'>
      <div className="group-btn-prod">
        <div className="d-grid gap-2 ms-3 col-2  ">
            <button type="button" className="btn btn-danger" onClick={() => handleCategoryClick('Mobilier de bureau')}>Mobilier De Bureau</button>
        </div>
        <div className="d-grid gap-2 ms-3 col-2  ">
            <button type="button" className="btn btn-danger" onClick={() => handleCategoryClick('Rayonnage')}>Rayonnage</button>
        </div>
        <div className="d-grid gap-2 ms-3 col-2  ">
            <button type="button" className="btn btn-danger" onClick={() => handleCategoryClick('Supermarche')}>Supermarché</button>
        </div>
        <div className="d-grid gap-2 ms-3 col-2  ">
            <button type="button" className="btn btn-danger" onClick={() => handleCategoryClick("Manutention aménagement d'espace")}>Manutention</button>
        </div>
      </div>
      <div>
        <h1 className='text-center mb-5'>Liste des produits</h1>
      </div>
      <table className='table table-striped-columns table-bordered'>
        <thead className=' text-center'>
          <tr>
            <th>Ref</th>
            <th>Nom du Produit</th>
            <th>Categorie</th>
            <th>Quantite</th>
            <th>Prix</th>
          </tr>
        </thead>
        <tbody>
          {produits.map(produit => (
            <tr key={produit.ref}>
              <td className=' text-center'>{produit.ref}</td>
              <td>{produit.prod_name}</td>
              <td>{produit.categorie}</td>
              <td className='text-center'>{produit.quantite}</td>
              <td className='text-center'>{produit.prix} DH</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};


export default Verification;



