import { defineConfig } from 'vite'
import react from '@vitejs/plugin-react'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
  server: {
    host: 'lmm.com'
  },

  
  build: {
    chunkSizeWarningLimit: 1600, // The size limit in KB
  }
});

