<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\AuthController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Api\ProduitController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// Route::post('/adduser',[AuthController::class, 'add_user']);

// Route::post('/logout',[AuthController::class, 'logout']);

Route::post('/login',[AuthController::class, 'login']); 
Route::post('/register',[AuthController::class, 'register']);
Route::get('/index',[AuthController::class, 'index']);
Route::get('/produits',[ProduitController::class, 'index']);
Route::get('/produits/category/{category}',[ProduitController::class, 'filterByCategory']);
Route::post('/produits/create',[ProduitController::class, 'register']);
Route::post('/produits/sell',[ProduitController::class, 'sell']);
Route::post('/historie',[ProduitController::class, 'historie']);
Route::put('/update/{ref}',[ProduitController::class, 'update']);
Route::put('/devis',[ProduitController::class, 'updateDevis']);
// Route::post('/sortie',[ProduitController::class, 'sortie']);
Route::delete('/delete/{ref}',[ProduitController::class, 'destroy']);
Route::post('/historie/record',[ProduitController::class, 'historie']);
Route::get('/historie/show',[ProduitController::class, 'indexHistorie']);
Route::post('/sortie',[ProduitController::class, 'sortie']);
Route::get('/sortie/index',[ProduitController::class, 'indexSortie']);

